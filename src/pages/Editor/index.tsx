import './editor.scss';
import '../../html5reset.css';

import 'whatwg-fetch';

import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Editor } from './components/Editor';

ReactDOM.render(
    <Editor />,
    document.getElementById("container")
);