import * as React from 'react';
const Draggable = require('react-draggable');

export interface WindowProps {
    display: boolean;
    title: string;
    content: React.ReactElement<undefined>;
    controls: React.ReactElement<undefined>;
}

export interface WindowState {

}

export class Window extends React.Component<WindowProps, WindowState> {


    public constructor(props) {
        super(props);
    }

    public render() {
        return <Draggable bounds='body' handle='.header'>
            <div className='window' style={{ display: this.props.display ? 'block' : 'none' }}>
                <div className='header'>{this.props.title}</div>
                <div className='content'>{this.props.content}</div>
                <div className='controls'>{this.props.controls}</div>
            </div>
        </Draggable>;
    }
}