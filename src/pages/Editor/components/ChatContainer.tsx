import * as React from 'react';

import { ChatController } from 'collab.io-client-api';
import { ControllerFactory } from '../../../factories/ControllerFactory';
import { WindowContainer, WindowContainerState } from './WindowContainer';
import { Chat } from './Chat';
import { ChatControls } from './ChatControls';

export interface ChatContainerState extends WindowContainerState { messages: string[]; message: string; }

export class ChatContainer extends WindowContainer<undefined, ChatContainerState> {

    public static title = 'Chat';

    protected controller: ChatController;

    public constructor(props?) {
        super(props);

        this.state = {
            messages: [],
            message: '',
            title: ChatContainer.title
        };

        this.controller = ControllerFactory.get(ChatController);

        this.controller.OnMessage(this.onMessage.bind(this));
    }

    public onMessage(msg) {
        this.setState(prevState => {
            let messages = prevState.messages.slice();
            messages.push(msg.text);
            return { messages };
        });
    }

    public send() {
        this.controller.SendMessage(this.state.message);
        this.onMessage({ text: this.state.message });
        this.state.message = '';
    }

    private setMessage(message) {
        this.setState({
            message
        });
    }

    getContent() {
        return <Chat messages={this.state.messages} />
    }

    getControls() {
        return <ChatControls message={this.state.message}
                             send={this.send.bind(this)}
                             setMessage={this.setMessage.bind(this)} />
    }
}