import * as React from 'react';

import { EditorController, SceneController, AssetsController, SceneNode, Mesh } from 'collab.io-client-api';

export interface AssetsControlsProps { uploadFile: (assetData: any) => Promise<any> }
export interface AssetsControlsState { }

export class AssetsControls extends React.Component<AssetsControlsProps, AssetsControlsState> {

    private handleUpload({ target }) {
        let files = target.files;

        if (files && files.length) {
            for (let file of files) {
                let parts = file.name.split('.');
                let reader = new FileReader();

                reader.onload = async (e: any) => {
                    await this.props.uploadFile({
                        name: file.name,
                        extension: parts[parts.length - 1],
                        data: e.target.result
                    });
                };
                reader.readAsDataURL(file);
            }
        }
    }

    render() {
        return <div>
            <input id="asset-upload" onChange={this.handleUpload.bind(this)} type="file" multiple />
            <label className="col-12" htmlFor="asset-upload">Import asset</label>
        </div>;
    }
}