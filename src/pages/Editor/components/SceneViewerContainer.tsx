import * as React from 'react';
import * as update from 'immutability-helper';

import { EditorController, SceneController, Scene, SceneNode, Project } from 'collab.io-client-api';
import { ControllerFactory } from '../../../factories/ControllerFactory';
import { WindowContainer, WindowContainerProps, WindowContainerState } from './WindowContainer';

export interface SceneViewerContainerProps extends WindowContainerProps { }
export interface SceneViewerContainerState extends WindowContainerState { scenes: Scene[] }

export class SceneViewerContainer extends WindowContainer<SceneViewerContainerProps, SceneViewerContainerState> {

    public static title = 'Scenes';

    public constructor(props: SceneViewerContainerProps) {
        super(props);

        this.state = {
            scenes: []
        };

        let editorController = ControllerFactory.get(EditorController);
        editorController.OnSceneAdded.Subscribe(scene => {
            let scenes = this.state.scenes.slice();
            scenes.push(scene);
            this.setState({ scenes });

            let sceneController = ControllerFactory.get(SceneController, scene._id);
            sceneController.OnNodeAdded.Subscribe((to, node) => {
                let sceneIndex = this.state.scenes.findIndex(s => s._id === scene._id);
                let treePath = n => {
                    if (n._id === to._id)
                        return { children: { $push: [update(node, { parent: { $set: n } })] } };
                    for (let i = 0; i < n.children.length; ++i) {
                        let r = treePath(n.children[i]);
                        if (r) {
                            return { children: { [i]: r } };
                        }
                    }
                    return null;
                };
                let path = treePath(this.state.scenes[sceneIndex].root);
                if (path) {
                    this.setState(state => 
                        ({ scenes: update(state.scenes, { [sceneIndex]: { root: path } }) })
                    );
                }
            });
            sceneController.OnNodeRemoved.Subscribe(node => {
                let sceneIndex = this.state.scenes.findIndex(s => s._id === scene._id);
                let treePath = n => {
                    for (let i = 0; i < n.children.length; ++i) {
                        if (n.children[i]._id === node._id) {
                            return { children: { $splice: [[i, 1]] } }
                        }

                        let r = treePath(n.children[i]);
                        if (r) {
                            return { children: { [i]: r } };
                        }
                    }
                    return null;
                };
                let path = treePath(this.state.scenes[sceneIndex].root);
                if (path) {
                    this.setState(state => 
                        ({ scenes: update(state.scenes, { [sceneIndex]: { root: path } }) })
                    );
                }
            });
        });
    }

    public addScene() {
        ControllerFactory.get(EditorController).BroadcastAddScene(SceneController.ConstructScene('New Scene'));
    }

    public moveNodeTo(sceneId: number, nodeId: string, toSceneId: number, toId: string) {
        if (!nodeId || !toId)
            return;

        let controller = ControllerFactory.get(EditorController);
        let sceneFrom = ControllerFactory.get(SceneController, sceneId);
        let sceneTo = ControllerFactory.get(SceneController, toSceneId);
        let node = sceneFrom.FindNodeById(nodeId);
        let to = sceneTo.FindNodeById(toId);

        sceneFrom.BroadcastRemoveNode(node);
        sceneTo.BroadcastAddNode(to, node)
    }

    getControls() {
        return <button onClick={this.addScene.bind(this)}>Add new scene</button>;
    }

    getContent() {
        return <div className="tree">
            {this.state.scenes.map(this.renderScene.bind(this))}
        </div>;
    }

    private renderScene(scene: Scene) {
        let header: HTMLDivElement;
        let container: HTMLDivElement;
        let visible = false;

        let toggle = () => {
            if (scene.root.children.length) {
                visible = !visible;
                header.classList.remove(visible ? 'closed' : 'opened');
                header.classList.add(visible ? 'opened' : 'closed');
                container.classList.toggle('hidden');
            }
        };

        return <div>
            <div onClick={toggle}
                ref={elem => header = elem}
                className={scene.root.children.length ? 'closed label' : 'label'}
                onDragOver={e => e.preventDefault()}
                onDrop={e => this.moveNodeTo(JSON.parse(e.dataTransfer.getData('text/json')).sceneId, JSON.parse(e.dataTransfer.getData('text/json')).nodeId, scene._id, scene.root._id)}>
                {scene.name}
            </div>
            <div ref={elem => container = elem} className="hidden">
                {scene.root.children.map(this.renderSceneTree.bind(this, scene))}
            </div>
        </div>;
    }

    private renderSceneTree(scene: Scene, node: SceneNode) {
        let header: HTMLDivElement;
        let container: HTMLDivElement;
        let visible = false;

        let toggle = () => {
            if (node.children.length) {
                visible = !visible;
                header.classList.remove(visible ? 'closed' : 'opened');
                header.classList.add(visible ? 'opened' : 'closed');
                container.classList.toggle('hidden');
            }
        };

        return <div className="indent"
            draggable={true}
            onDragStart={e => e.dataTransfer.setData('text/json', JSON.stringify({ sceneId: scene._id, nodeId: node._id.toString() }))}>
            <div onClick={toggle}
                onDragOver={e => e.preventDefault()}
                onDrop={e => this.moveNodeTo(JSON.parse(e.dataTransfer.getData('text/json')).sceneId, JSON.parse(e.dataTransfer.getData('text/json')).nodeId, scene._id, node._id)}
                ref={elem => header = elem} className={node.children.length ? 'closed label' : 'label'}>{node.name}</div>
            <div ref={elem => container = elem} className="children hidden">
                {node.children.map(child => this.renderSceneTree(scene, child))}
            </div>
        </div>
    }
}