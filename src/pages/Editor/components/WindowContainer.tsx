import * as React from 'react';

import { Window } from './Window';
import { Scene } from 'collab.io-client-api';

export interface WindowContainerProps {
    activeScene: Scene;
}

export interface WindowContainerState {
    display: boolean;
    title: string;
}

export class WindowContainer<P extends WindowContainerProps, S extends WindowContainerState> extends React.Component<P, any> {
    
    public static title;

    public constructor(props) {
        super(props);

        this.state = {
            title: WindowContainer.title,
            display: false
        };
    }

    toggle() {
        this.setState({ display: !this.state.display });
    }

    getContent() {
        return null;
    }

    getControls() {
        return null;
    }

    public render() {
        return <Window title={this.state.title} display={this.state.display} content={this.getContent()} controls={this.getControls()} />
    }
}