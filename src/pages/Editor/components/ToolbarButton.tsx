import * as React from 'react';

export interface ToolbarButtonProps { label: string; onClick: () => void; }

export class ToolbarButton extends React.Component<ToolbarButtonProps, undefined> {

    render() {
        return <button onClick={this.props.onClick}>{this.props.label}</button>;
    }
}