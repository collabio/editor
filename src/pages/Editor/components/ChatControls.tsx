import * as React from 'react';

export interface ChatControlsProps {
    send: () => void;
    setMessage: (string) => void;
    message: string;
}

export class ChatControls extends React.Component<ChatControlsProps, undefined> {

    private sendMessage(e) {
        e.preventDefault();
        this.props.send();
    }

    private handleMessageChange({ target }) {
        this.props.setMessage(target.value);
    }
    
    public render() {
        return <form onSubmit={this.sendMessage.bind(this)}>
            <input className="col-10" placeholder="message" value={this.props.message} onChange={this.handleMessageChange.bind(this)} />
            <input className="col-2" type="submit" value="send" />
        </form>
    }
}