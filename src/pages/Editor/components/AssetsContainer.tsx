import * as React from 'react';

import { SceneController, AssetsController, SceneNode, Mesh } from 'collab.io-client-api';
import { ControllerFactory } from '../../../factories/ControllerFactory';
import { WindowContainer, WindowContainerProps, WindowContainerState } from './WindowContainer';
import { Assets } from './Assets';
import { AssetsControls } from './AssetsControls';

export interface AssetsContainerProps extends WindowContainerProps { }
export interface AssetsContainerState extends WindowContainerState { assets: Mesh[] }

export class AssetsContainer extends WindowContainer<AssetsContainerProps, AssetsContainerState> {

    public static title = 'Assets';

    protected assetsController: AssetsController;

    public constructor(props: AssetsContainerProps) {
        super(props);

        this.assetsController = ControllerFactory.get(AssetsController);

        this.state = {
            title: AssetsContainer.title,
            assets: this.assetsController.List.slice()
        };

        this.assetsController.OnAssetAdded.Subscribe(asset => {
            let newAssets = this.state.assets.slice();
            newAssets.push(asset);
            this.setState({ assets: newAssets });
        });
    }

    public addAssetToScene(asset: Mesh) {
        let node: SceneNode = SceneController.ConstructNode(asset.name + Math.floor(Math.random() * 10));
        node.mesh = asset;

        let scene = ControllerFactory.get(SceneController, this.props.activeScene._id);
        scene.BroadcastAddNode(scene.Root, node);
    }

    public async uploadFile(assetData) {
        return await this.assetsController.Upload(assetData);
    }

    getContent() {
        return <Assets assets={this.state.assets} addAssetToScene={this.addAssetToScene.bind(this)} />;
    }

    getControls() {
        return <AssetsControls uploadFile={this.uploadFile.bind(this)} />;
    }
}