import * as React from 'react';
import * as BABYLON from 'import-babylonjs';

import { EditorController, SceneController, Scene, SceneNode } from 'collab.io-client-api';
import { ControllerFactory } from '../../../factories/ControllerFactory';

export interface CanvasContentProps { scene: Scene; isActive: boolean; }
export interface CanvasContentState { }

export class CanvasContent extends React.Component<CanvasContentProps, CanvasContentState> {

    private canvas: HTMLCanvasElement;
    private controller: SceneController;
    private engine: BABYLON.Engine;
    private scene: BABYLON.Scene;
    private loader: BABYLON.AssetsManager;

    public constructor(props) {
        super(props);

        this.controller = ControllerFactory.get(SceneController, props.scene._id);

        if (this.props.isActive) {
            this.engine.runRenderLoop(() => {
                this.scene.render();
            });
        }
    }

    public initialize() {
        this.engine = new BABYLON.Engine(this.canvas, false);

        this.scene = new BABYLON.Scene(this.engine);
        this.loader = new BABYLON.AssetsManager(this.scene);
        this.loader.useDefaultLoadingScreen = false;

        this.scene.clearColor = new BABYLON.Color3(0, 0, 0);
        var camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 5, -10), this.scene);

        // target the camera to scene origin
        camera.setTarget(BABYLON.Vector3.Zero());

        // attach the camera to the canvas
        camera.attachControl(this.canvas, false);

        // create a basic light, aiming 0,1,0 - meaning, to the sky
        var light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), this.scene);

        this.controller.OnNodeAdded.Subscribe((parent, child) => {
            this.newNode(parent, child);
        });
        this.controller.OnNodeRemoved.Subscribe(node => {
            this.removeNode(node);
        });
        //this.controller.OnActivation.Subscribe(this.activate.bind(this));
        //this.controller.OnDeactivation.Subscribe(this.deactivate.bind(this));
    }

    public componentWillReceiveProps(nextProps) {
        if (this.props.isActive && !nextProps.isActive) {

            this.engine.stopRenderLoop();
        }
        else if (!this.props.isActive && nextProps.isActive) {
            this.engine.runRenderLoop(() => {
                this.scene.render();
            });
        }
    }

    public newNode(parent: SceneNode, node: SceneNode) {
        let position = new BABYLON.Vector3(node.position.x, node.position.y, node.position.z);
        node.position = position;

        var container = new BABYLON.Mesh(node._id.toString(), this.scene, this.scene.getMeshByName(parent._id.toString()));
        container.position = position;

        this.loader.reset();

        let slash = node.mesh.url.lastIndexOf('/') + 1;
        let path = node.mesh.url.substr(0, slash);
        let filename = node.mesh.url.substr(slash);
        var task = this.loader.addMeshTask(node.mesh.name, '', path, filename);
        task.onSuccess = (t: any) => {
            t.loadedMeshes.forEach((mesh: BABYLON.Mesh) => {
                mesh.parent = container;
            });
        };

        this.loader.load();

        for (let child of node.children)
            this.newNode(node, child);
    }

    public removeNode(node: SceneNode) {
        let mesh = this.scene.getMeshByName(node._id.toString());

        let recurrency = (item: BABYLON.AbstractMesh) => {
            for (let child of item.getChildMeshes()) {
                recurrency(child);
            }
            this.scene.removeMesh(item);
        }

        recurrency(mesh);
    }

    public componentDidMount() {
        window.addEventListener('resize', this.resize.bind(this));

        this.initialize();
    }

    public componentDidUpdate() {
        this.resize();
    }

    public componentWillUnmount() {
        window.removeEventListener('resize', this.resize.bind(this));
    }

    private resize() {
        this.engine.resize();
    }

    public render() {
        let classes = ['maximized'];

        if (this.props.isActive === false) {
            classes.push('hidden');
        }

        return <canvas ref={c => this.canvas = c} className={classes.join(' ')}></canvas>;
    }
}