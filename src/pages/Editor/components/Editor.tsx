import * as React from 'react';

import { EditorController, Project, Scene, SceneNode } from 'collab.io-client-api';
import { ControllerFactory } from '../../../factories/ControllerFactory';

import { WindowContainer, WindowContainerProps, WindowContainerState } from './WindowContainer';
import { ChatContainer } from './ChatContainer';
import { AssetsContainer } from './AssetsContainer';
import { SceneViewerContainer } from './SceneViewerContainer';
import { CanvasContent } from './CanvasContent';

import { ToolbarButton } from './ToolbarButton';

export interface EditorState {
    project?: Project;
    windows?: any[];
    scenes?: Scene[];
    activeScene?: Scene;
}

export class Editor extends React.Component<undefined, EditorState> {

    private controller: EditorController;

    public constructor() {
        super();

        this.controller = ControllerFactory.get(EditorController);

        this.state = {
            project: null,
            windows: [ChatContainer, AssetsContainer, SceneViewerContainer],//, new AssetsWindow(), new SceneViewerWindow(), new PropertiesWindow()],
            scenes: [],
            activeScene: null
        };

        this.controller.LoadProject()
            .then(project => this.setState({ project }));

        this.controller.OnSceneAdded.Subscribe(scene => {
            let scenes = this.state.scenes.slice();
            scenes.push(scene);
            this.setState({ scenes });
        });
    }

    public selectScene(scene: Scene) {
        this.setState({ activeScene: scene });
    }

    render() {
        let windows = {};

        if (this.state.project)
            window.document.title = `${this.state.project.name} - collab.io`;

        return <section id="editor">
            <div id="windows">
                {this.state.windows.map((window, i) => 
                    React.createElement<any>(window, { activeScene: this.state.activeScene, ref: r => windows[i] = r })
                )}
            </div>
            <section id="main-menu">
                <div>Project</div>
                <div>Edit</div>
                <div>About</div>
            </section>
            <section id="top-toolbar"></section>
            <section id="left-toolbar">
                <div className="rotate-90deg-right">
                </div>
            </section>
            <section id="tabs-menu">
                {this.state.project ? this.state.project.scenes.map(scene =>
                    <div className="tab" onClick={this.selectScene.bind(this, scene)}>{scene.name}<span className="close">X</span></div>
                ) : null}
            </section>
            <section id="content">
                {this.state.scenes.map(scene =>
                    <CanvasContent scene={scene} isActive={scene === this.state.activeScene} />
                )}
            </section>
            <section id="right-toolbar">
                <div className="rotate-90deg-right">
                    {this.state.windows.map((window, i) =>
                        <ToolbarButton label={window.title} onClick={() => windows[i] && windows[i].toggle()}></ToolbarButton>
                    )}
                </div>
            </section>
            <section id="status-bar">
                <section id="test">
                </section>
            </section>
        </section>;
    }
}