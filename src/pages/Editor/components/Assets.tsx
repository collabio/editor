import * as React from 'react';

import { EditorController, SceneController, AssetsController, SceneNode, Mesh } from 'collab.io-client-api';

export interface AssetsProps { assets: Mesh[]; addAssetToScene: (asset: Mesh) => void }
export interface AssetsState { }

export class Assets extends React.Component<AssetsProps, AssetsState> {

    private addAssetToScene(asset: Mesh) {
        this.props.addAssetToScene(asset);
    }

    render() {
        return <div>
            {this.props.assets.map(asset =>
                <div onClick={this.addAssetToScene.bind(this, asset)}>{asset.name}</div>
            )}
        </div>;
    }
}