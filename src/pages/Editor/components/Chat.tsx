import * as React from 'react';

export interface ChatProps {
    messages: string[];
}

export class Chat extends React.Component<ChatProps, undefined> {
    public render() {
        return <div>
            {this.props.messages.map(message =>
                <div>{message}</div>
            )}
        </div>
    }
}