import '../../html5reset.css';
import './main.scss';

import 'whatwg-fetch';

import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Main } from './components/Main';
//import { Manage } from "./pages/Manage";

ReactDOM.render(
    <Main />,
    document.getElementById("container")
);