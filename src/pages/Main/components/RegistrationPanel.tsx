import * as React from 'react';
import * as Cookies from 'js-cookie';

export interface RegistrationPanelProps { onError: (message: string) => void; onRegistration?: () => void; }

export interface RegistrationPanelState { login?: string; password?: string; }

function register(login: string, password: string): Promise<void> {
    return fetch('/api/users', {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json; charset=utf-8' },
        body: JSON.stringify({ login, password })
    })
    .then<any>(response => response.json())
    .then(data => data.error ? Promise.reject(data.error) : data);
}

export class RegistrationPanel extends React.Component<RegistrationPanelProps, RegistrationPanelState> {

    public constructor(props) {
        super(props);

        this.state = {
            login: '',
            password: ''
        };
    }

    public submit(event) {
        event.preventDefault();

        register(this.state.login, this.state.password)
        .then(this.props.onRegistration)
        .catch(() => this.props.onError('Registration failed.'));
    }

    private handleLoginChange(event) {
        this.setState({
            login: event.target.value
        });
    }

    private handlePasswordChange(event) {
        this.setState({
            password: event.target.value
        });
    }

    public render() {
        return <form onSubmit={this.submit.bind(this)} className='panel'>
            <input placeholder="login" value={this.state.login} onChange={this.handleLoginChange.bind(this)} />
            <input placeholder="password" type="password" value={this.state.password} onChange={this.handlePasswordChange.bind(this)} />
            <input type="submit" value="register" />
        </form>;
    }
}