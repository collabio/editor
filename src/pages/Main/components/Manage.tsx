import * as React from 'react';
import * as Cookies from 'js-cookie';


import { ProjectList } from './ProjectList';
import { EditProject } from './EditProject';

export interface ManageProps { onLogout: () => void }
export interface ManageState { editingProject: any }

interface Project {
    _id: number;
    name: string;
}

function addProject(): Promise<void> {
    return fetch('/api/projects', {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json; charset=utf-8' },
        credentials: 'include',
        body: JSON.stringify({ name: 'New project', scenes: [], members: [] })
    })
        .then<any>(response => response.json())
        .then(data => data.error ? Promise.reject(data.error) : data)
}

export class Manage extends React.Component<ManageProps, ManageState> {

    private list: ProjectList;

    public constructor(props) {
        super(props);

        this.state = {
            editingProject: null
        };
    }

    public logout() {
        this.props.onLogout();
    }

    public addProject() {
        addProject()
            .then(() => this.list.update());
    }

    public selectProject(project: Project) {
        this.setState({ editingProject: project });
    }

    public render() {
        return <div>
            <div className="bar">
                <h1>collab.io</h1>
                <button onClick={this.logout.bind(this)}>log out</button>
                <button onClick={this.addProject.bind(this)}>add project</button>
            </div>
            <ProjectList ref={list => this.list = list} halved={this.state.editingProject !== null} onProjectSelected={this.selectProject.bind(this)} />
            {this.state.editingProject !== null ?
                <EditProject project={this.state.editingProject} onUpdate={this.list.update.bind(this.list)} /> : null}
        </div>;
    }
}