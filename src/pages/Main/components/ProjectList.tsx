import * as React from 'react';

export interface ProjectListProps { halved: boolean, onProjectSelected: (project: Project) => void  }
export interface ProjectListState { projects: Project[] }

interface Project {
    _id: number;
    name: string;
}

function getProjects(): Promise<Project[]> {
    return fetch('/api/projects', {
        method: 'GET',
        headers: { 'Content-Type': 'application/json; charset=utf-8' },
        credentials: 'include'
    })
        .then<any>(response => response.json())
        .then(data => data.error ? Promise.reject(data.error) : data)
}

function deleteProject(project: Project): Promise<void> {
    return fetch(`/api/projects/${project._id}`, {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json; charset=utf-8' },
        credentials: 'include'
    })
        .then<any>(response => response.json())
        .then(data => data.error ? Promise.reject(data.error) : data)
}

export class ProjectList extends React.Component<ProjectListProps, ProjectListState> {

    public constructor(props) {
        super(props);

        this.state = {
            projects: []
        };

        this.update();
    }

    public async update() {
        let projects = await getProjects();
        this.setState({ projects });
    }

    public open(project: Project) {
        window.location.href = `/editor/project/${project._id}`;
    }

    public edit(project: Project) {
        this.props.onProjectSelected(project);
    }

    public async delete(project: Project) {
        await deleteProject(project);
        this.update();
    }

    public render() {
        return <table className={this.props.halved ? 'halved' : null}>
            {this.state.projects.length === 0 ? <div>There are no projects</div> : null}
            {this.state.projects.map(project =>
                <tr>
                    <td style={{ width: '70%' }}>{project.name}</td>
                    <td style={{ width: '10%' }}><button onClick={this.open.bind(this, project)}>open</button></td>
                    <td style={{ width: '10%' }}><button onClick={this.edit.bind(this, project)}>edit</button></td>
                    <td style={{ width: '10%' }}><button onClick={this.delete.bind(this, project)}>delete</button></td>
                </tr>)}
        </table>;
    }
}