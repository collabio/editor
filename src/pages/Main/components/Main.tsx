import * as React from 'react';
import { toByteArray as Base64toArray } from 'base64-js';
import * as Cookies from 'js-cookie';

import { LoginPanel } from './LoginPanel';
import { RegistrationPanel } from './RegistrationPanel';
import { Manage } from './Manage';

export interface User {
    _id: number;
    name: string;
}
export interface MainState { user?: User, message?: string }

function getUser(id: number): Promise<User> {
    return fetch(`/api/users/${id}`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json; charset=utf-8' },
        credentials: 'include'
    })
        .then<any>(response => response.json())
        .then(data => data.error ? Promise.reject(data.error) : data)
}

export class Main extends React.Component<undefined, MainState> {

    public constructor(props) {
        super(props);

        this.state = {
            user: null,
            message: null
        };

        this.reload();
    }

    public logout() {
        Cookies.remove('authToken');
        this.setState({ user: null });
    }

    public reload() {
        let authToken = Cookies.get('authToken');
        if (authToken) {
            let userId = Base64toArray(authToken).slice(0, 4).reduce((p, c, i) => p + Math.pow(c, 4 - i));

            getUser(userId)
                .then(user => user._id ? this.setState({ user }) : this.logout());
        }
    }

    public showError(message: string) {
        this.setState({ message });
    }

    public showSuccess() {
        let message = 'Rejestracja się powiodła! Możesz teraz się zalogować.';
        this.setState({ message });
    }

    public render() {
        return this.state.user ? <Manage onLogout={this.logout.bind(this)} />
            : (<div className="main">
                <h1 className="logo">collab.io</h1><div>
                    <div className="message">{this.state.message}</div>
                    <LoginPanel onError={this.showError.bind(this)} onLogin={this.reload.bind(this)} />
                    <div>or</div>
                    <RegistrationPanel onError={this.showError.bind(this)} onRegistration={this.showSuccess.bind(this)} />
                </div>
            </div>);
    }
}