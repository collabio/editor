import * as React from 'react';

export interface EditProjectProps { project: Project, onUpdate: () => void }
export interface EditProjectState { project?: Project, newMember?}

interface Project {
    _id?: number;
    name?: string;
    members?: number[];
}

function saveProject(project): Promise<Project[]> {
    return fetch(`/api/projects/${project._id}`, {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/json; charset=utf-8' },
        credentials: 'include',
        body: JSON.stringify(project)
    })
        .then<any>(response => response.json())
        .then(data => data.error ? Promise.reject(data.error) : data)
}

export class EditProject extends React.Component<EditProjectProps, EditProjectState> {

    public constructor(props) {
        super(props);

        this.state = { project: props.project, newMember: '' };
    }

    public componentWillReceiveProps(props) {
        this.state = { project: props.project, newMember: '' };
    }    

    public handleNameChange(event) {
        this.setState({
            project: {
                name: event.target.value
            }
        });
    }

    public handleMemberChange(event) {
        this.setState({ newMember: event.target.value });
    }

    public addMember() {
        if(!this.state.newMember)
            return;
            
        let members = this.state.project.members.slice();

        members.push(this.state.newMember);
        this.setState({ project: { _id: this.state.project._id, name: this.state.project.name, members }, newMember: '' });
    }

    public deleteMember(member) {
        let members = this.state.project.members.slice();

        members.splice(this.state.project.members.indexOf(member), 1);
        this.setState({ project: { _id: this.state.project._id, name: this.state.project.name, members } });
    }

    public async save() {
        await saveProject(this.state.project);
        this.props.onUpdate();
    }

    public render() {
        return <table className="halved">
            <tr>
                <td>Id:</td>
                <td><input value={this.state.project._id} disabled /></td>
            </tr>
            <tr>
                <td>Name:</td>
                <td><input value={this.state.project.name} onChange={this.handleNameChange.bind(this)} /></td>
            </tr>
            <tr>
                <td>Members:</td>
                <td>
                    {this.state.project.members.map(member =>
                        <div className="member">{member}<span className="delete" onClick={this.deleteMember.bind(this, member)}>X</span></div>)
                    }
                    <input className="member-input" value={this.state.newMember} onChange={this.handleMemberChange.bind(this)} />
                    <button className="member-button" onClick={this.addMember.bind(this)}>Dodaj</button>
                </td>
            </tr>
            <tr>
                <td colSpan={2}><button onClick={this.save.bind(this)}>Save</button></td>
            </tr>
        </table>;
    }
}