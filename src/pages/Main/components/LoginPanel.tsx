import * as React from 'react';
import * as Cookies from 'js-cookie';

export interface LoginPanelProps { onError: (message: string) => void; onLogin?: () => void; }

export interface LoginPanelState { login?: string; password?: string; }

function login(login: string, password: string): Promise<string> {
    return fetch('/api/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json; charset=utf-8' },
        body: JSON.stringify({ login, password })
    })
    .then<any>(response => response.json())
    .then(data => data.error ? Promise.reject(data.error) : data)
    .then(data => data.authToken);
}

export class LoginPanel extends React.Component<LoginPanelProps, LoginPanelState> {

    public constructor(props) {
        super(props);

        this.state = {
            login: '',
            password: ''
        };
    }

    public submit(event) {
        event.preventDefault();

        login(this.state.login, this.state.password)
        .then(token => Cookies.set('authToken', token))
        .then(this.props.onLogin)
        .catch(() => this.props.onError('Invalid login data.'));
    }

    private handleLoginChange(event) {
        this.setState({
            login: event.target.value
        });
    }

    private handlePasswordChange(event) {
        this.setState({
            password: event.target.value
        });
    }

    public render() {
        return <form onSubmit={this.submit.bind(this)} className='panel'>
            <input placeholder="login" value={this.state.login} onChange={this.handleLoginChange.bind(this)} />
            <input placeholder="password" type="password" value={this.state.password} onChange={this.handlePasswordChange.bind(this)} />
            <input type="submit" value="log in" />
        </form>;
    }
}