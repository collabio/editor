import { ApiClient as Client, BrowserHttpClient } from 'collab.io-networking';

export class ApiClient {
    private static client: Client;

    public static get Instance(): Client {
        return this.client ? this.client
                           : this.client = new Client(new BrowserHttpClient(), '/api/');
    }
}