import { RtcClient, WsClient, BrowserRtcFactory, Client } from 'collab.io-networking';

export class NetworkingClient {
    private static client: Client;

    public static get Instance(): Client {
        return this.client ? this.client
                           : this.client = WsClient.Get('/relay');
         //                  : this.client = RtcClient.Get('/stun', new BrowserRtcFactory());
    }
}