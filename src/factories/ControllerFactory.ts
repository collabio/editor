import { EditorController, SceneController, AssetsController, ChatController } from 'collab.io-client-api';
import { NetworkingClient } from '../NetworkingClient';
import { ApiClient } from '../ApiClient';
import { UrlUtils } from '../utils/UrlUtils';

let networkingClient = NetworkingClient.Instance;
let apiClient = ApiClient.Instance;
let projectId = UrlUtils.GetProjectId();

let cache = {};

export class ControllerFactory {
    public static get<T>(type: { new(...args): T }, data?): T {
        let key = JSON.stringify({ class: type.name, data });
        if (cache[key]) {
            return cache[key];
        }

        let controller;
        switch (<any>type) {
            case EditorController:
                controller = new type(networkingClient, apiClient, projectId);
                break;

            case SceneController:
                controller = new type(apiClient, networkingClient, projectId, data);
                break;

            case AssetsController:
                controller = new type(apiClient, networkingClient, projectId);
                break;

            case ChatController:
                controller = new type(networkingClient);
                break;
        }

        cache[key] = controller;
        return controller;
    }
}