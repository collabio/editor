var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        "main": "./src/pages/Main/index.tsx",
        "editor": "./src/pages/Editor/index.tsx"
    },
    output: {
        filename: "[name].bundle.js",
        path: __dirname + "/dist"
    },

    devtool: "source-map",

    resolve: {
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        loaders: [
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
            {
                test: /\.scss$/,
                loaders: ["style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.css$/,
                loaders: ["style-loader", "css-loader"]
            }
        ]
    },

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
        "react": "React",
        "react-dom": "ReactDOM",

        //change it to just import-babylonjs
        "import-babylonjs": "BABYLON"
    },

    plugins: [
        new CopyWebpackPlugin([
            { from: "./src/pages/Editor/editor.html", to: "./editor.html" },
            { from: "./src/pages/Main/main.html", to: "./main.html" },
            { from: "./node_modules/react/dist/react.min.js", to: "./react.min.js" },
            { from: "./node_modules/react-dom/dist/react-dom.min.js", to: "./react-dom.min.js" },
            //change it to node_modules/import-babylonjs
            { from: "../import-babylonjs/babylon.js", to: "./babylon.js" }
        ])
    ]
};